var gulp = require('gulp');
var pug = require('gulp-pug');
var sass = require('gulp-sass');
var babel = require('gulp-babel');
var browserify = require('gulp-browserify');
var browserSync = require('browser-sync').create();	

// Static server
gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
});

gulp.task('pug', function() {
  return gulp.src('./src/view/index.pug')
  	.pipe(pug({
  		pretty: true
  	}))
  	.pipe(gulp.dest('./dist/'));
});

gulp.task('sass', function() {
  return gulp.src('./src/sass/app.scss')
  	.pipe(sass().on('error', sass.logError))
  	.pipe(sass({outputStyle: 'compressed'}))
  	.pipe(gulp.dest('./dist/css/'));
});

gulp.task('js', function() {
  return gulp.src('./src/js/app.js')
  	.pipe(babel({
  		presets:['@babel/env']
  	}))
  	.pipe(browserify())
  	.pipe(gulp.dest('./dist/js/'));
});


// Static Server + watching scss/html files
gulp.task('srv',['pug','sass','js'], function() {

    browserSync.init({
        server: "./dist"
    });

    gulp.watch("./src/sass/**/*.scss", ['sass']);
    gulp.watch("./src/view/**/*.pug", ['pug']);
    gulp.watch("./src/js/**/*.js", ['js']);

    gulp.watch("dist/**/*.*").on('change', browserSync.reload);
});